import os
import httpx
import json
import motor.motor_asyncio

from typing import Union

from fastapi import  FastAPI,Response,Request,Header,Path
from fastapi.responses import JSONResponse
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder

from pydantic import BaseModel, Field
from typing import Optional, List
from bson import ObjectId

app = FastAPI()

url_api_mercado_libre = os.environ["URL_MERCADO_LIBRE"]

config_request = os.environ["CONFIG_CLIENT"]
# conect to mongo
client = motor.motor_asyncio.AsyncIOMotorClient(os.environ["MONGO_DB"])
db = client.requests
request_collection = db.get_collection("requests_collection")





class Client(BaseModel):
    
    client_key: str = Field()
    client_id: int = Field(...)
    ip: str = Field(...)
    path: str = Field(...)
    status_code: int = Field(...)
    

@app.get("/categories/{categorie_id}")
async def read_categories(
    request: Request,
    categorie_id: str=Path(max_length=10),
    client_key: str = Header(convert_underscores=False,min_length=1,max_length=8),
    client_id: int = Header(convert_underscores=False,gt=1,lt=9999999),
):
    # TODO add logs
    # start_time = time.time()
    client_ip =  request.client.host
    path = request.url.path
    params = {"categorie_id":categorie_id}
    callback_url = "%s/%s" % (url_api_mercado_libre,categorie_id)
    r = httpx.get(callback_url)
    status_code = r.status_code
    client = Client(
                client_key=client_key,
                client_id=client_id,
                ip=client_ip,
                path=path,
                status_code=status_code

    )
    client_enconde = jsonable_encoder(client)

    new_client = await  request_collection.insert_one(client_enconde)
   
    return Response(content=r.text,media_type="application/json")
   
@app.middleware("http")
async def before_process(request: Request, call_next):
 
    
    config_json = json.loads(config_request)
    config = config_json
    request_client = await request_collection.count_documents(
                                                            {
                                                                'client_key':config.get("client_key"),
                                                                'client_id':config.get("client_id"),
                                                                'path':{ "$regex":config.get("path")},
                                                                'ip':config.get("ip"),
                                                            }
                                                            )
    client_ip = request.client.host
    header_client_id = request.headers['client_id']
    header_client_key = request.headers['client_key']
    
    if(client_ip != config.get("ip") or int(header_client_id) != config.get("client_id") or header_client_key != config.get("client_key")):
        return JSONResponse(status_code=400,content={'reason': "client do not exist"}) 
    if( request_client<=config.get("rate_limit",1)):
        response = await call_next(request)
        return response
    else:
        # TODO: save request for grafana
        return JSONResponse(status_code=429,content={'reason': "limit Exceeded or client do not exist"}) 

   