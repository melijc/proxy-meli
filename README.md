Proxy Meli
===

El presente proyecto es una api proxy que permite la comunicación desde un cliente externo hacia la api de mercadolibre, tiene control en el limite de peticiones, ip y path de llamada

Requerimientos para correr aplicación
===

1. Instalado docker 
2. Un mongoDB (deseable cluster)
3. Dar buenos feedback de este lindo proyecto


Pasos para correr aplicacion

0. Después de clonar el proyecto ingresa a tu directorio de destino y hace correr docker, todos los pasos siguientes son dentro del contenedor

```
docker run -it --rm --name proxy-meli  -v "$PWD":/usr/src/myapp -w /usr/src/myapp python:3 /bin/bash 
```


1. Crear el archivo .env desde .env_template 

```
$ copy .env_template .env
````

2. Definir variables de entorno en el .env copiado punto 1 (te dejo un ejemplo para guiarte, recuerda indicar las ip que corresponde)

```
export MONGO_DB=mongodb://172.17.0.4/client.proxy?retryWrites=true&w=majority
export URL_MERCADO_LIBRE=https://api.mercadolibre.com/categories
export CONFIG_CLIENT='{"client_key":"13abc","client_id":12123,"path":"^\\/categories\\/*","ip":"127.0.0.1","rate_limit":100}'
```
3. Un pequeño hack :) hace un source de tu .env 

```
source .env
```

4. Instala las dependencias

```
pip install -r requirements.txt
```

5. Hace correr el proyecto

```
uvicorn main:app --host 0.0.0.0 --reload
```

Alcances
===

Se Construirá una api de proxy de comunicación, la configuración de cliente será por variable de entorno (puede quedar en futuras implementaciones ser dispuesta en repositorio de información distinto a mongodb como mariadb, sqlserver, etc.)

Cómo se construyó??
===

Se diseño la solución a partir de un diagrama en C4, considerando las 3 primeras capas que entregan un mayor valor a lo que se quiere abordar

1. Capa Contexto

Según lo solicitado se contextualiza la solución que incluye peticiones de un cliente (host, mobile u otro) hacia el proxy construido.

![alt c4ccontexto](img/001-c4contexto.png)


2. Capa contenedores

Realizando un zoom en la solución MeliProxy, se despliega un detalle de los componentes que interactúan con la solución. Se considera un cache a través de REDIS el cual permitirá hacer un cache de las peticiones. La información de las peticiones se almacenará en un MongoDB, este es ideal que se encuentre en un cluster con alta disponibilidad. La solución que permitirá la comunicación de los distintos componentes se realizará en Python 3.0 con framework FastAPI el cual se encargará de controlar administrar las peticiones desde cliente externos hacia la api de Mercado Libre. Por último y no menos importante se contará con un Grafana para el monitoreo y seguimientos de las peticiones de los distintos clientes.

![alt c4contenedor](img/002-c4contenedores.png)

3. Capa componente

Acercándose más al detalle de cómo se implementará la solución, la capa de componente tiene el detalle de la iteración misma de la solución. Se observa que se contará con un controlador que permitirá el ingreso de cada petición. El controlador se encargará de realizar la petición hacia la api de Mercado Libre, cada petición pasará por un middleware encargado de verificar si el cliente es el que se encuentra registrado y si las peticiones no sobrepasan el máximo, lo anterior para proteger la api de Mercado libre y no se vea sobrecargada en posible ataques de denegación de servicio (DOS).

Lo declarado en esta capa es lo que construirá como solución 

![alt c4componente](img/003-c4componente.png)


Casos de usos
===

1. Considerando que pueden existir periodos de baja carga, se propone contar con un orquestador, en esta oportunidad y solo a modo de ejemplo se propone contar con docker compose, este es simple y permite hacer un escalamiento horizontal muy fácil, se podría considerar métricas como cantidad de solicitudes recibidas. Otra decisión de escalamiento podría ser por memoria asociada al componente de cache

![alt bajademanda](img/005-bajacarga.png)

2. Si la demanda por el consumo de proxy se ve incrementada y según lo explicado en el punto anterior se recomienda crecer horizontalmente de tal manera poder crecer de forma eficiente y eficaz, para que la propuesta de escalamiento de resultado, es recomendable dejar en cluster mongoDB y Redis para no tener puntos único de falla, si no llegase escalar horizontalmente se podría realizar un escalamiento vertical para poder asegurar recursos de respuesta. 
Todo lo anterior siempre monitoreado por Grafana, para tomar mejor decisión. 
Esta arquitectura permite que cuando el proceso de alta demanda, fechas como navidad, responda eficientemente y luego decrecer en baja demanda, permitiendo ahorrar recursos y consecuentemente dinero de facturación


![alt altademanda](img/006-altacarga.png)




Pruebas de carga
===

Se realizaron pruebas de carga si aplicar ningún tipo de mejora, ni cache, consumiendo respuesta directa de la api de mercadolibre y los resultado son los expuestos en el siguiente imagen

![alt altademanda](img/007-carga.png)

Para mejorar las pruebas de carga se pueden tomar varias medidas.

Primero, adjuntar el cache, ya pensado en primera instancia.
 
Segundo, cambiar el lenguaje Go (Golang) que es un lenguaje con mejor performance.

